# Hypertext Jeopardy Protocol

A C implementation of [RFC 8565 HTJP/1.0](https://tools.ietf.org/html/rfc8565). Writing the protocol in C allows us to target the transport layer and building up from there. 

Jeoprady is a library for app development using this protocol in a variaty of languages and servers.

Jeoprady is setup to take a chunk of data and return an HTTP request or query related to the endpoint in JSON format. The usefulness of this exchange is to setup a testing framework for APIs that serve HTTP requests and query middleware (graphQL, OData, etc.). This ensures that the API is serving the correct responses at the right endpoints and can even aid in troubleshooting responses in a stateful manner. There might be other uses so let your imagination go wild. 

Currently HTJP contains definitions for the following methods:

- CLUE -> This method is initiated by the client and is accepted by the server and recieves the junk of data like a POST in HTTP.
- DOUBLE -> This method is currently unused but should act the same as CLUE with double the point value, concept TBD.
- RESET -> This method resets the state management system of the server
- FINAL -> This method acts the same as CLUE but also resets the state management like the RESET method after a response is given to the client.
- LECTERN -> This method does not accept any data but returns identifying information about the server such as name, OS, point value, clues given since last reset, etc. configured in the server application.

We realize that this protocol started as an april fools joke but why not continue the fun? Keep it light and have fun with it. We hope you enjoy!

We are currently going over ways to add status codes much like the money/point value for clues in a game of Jeoprady. Suggestions and assistance with development are greatly appreciated.